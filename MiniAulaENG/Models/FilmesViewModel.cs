﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MiniAulaENG.Models {
    public class FilmesViewModel {

        public class CreateFilmeViewModel {
            [Required]
            [Display(Name = "Nome")]
            public string Nome { get; set; }

            [Required]
            [Display(Name = "Produtora")]
            public string Produtora { get; set; }

            [Required]
            [Display(Name = "Categoria")]
            public List<string> Categoria { get; set; }

            public List<SelectListItem> ProdutorasItens = new List<SelectListItem>();
            public List<Produtora> Produtoras {
                get {
                    var list = new List<Produtora>();
                    using (var db = new ModelContainer()) {
                        foreach (var i in ProdutorasItens) {
                            list.Add(db.Produtoras.Single(o=> o.Nome == i.Value));
                        }
                    }
                    return list;
                }
                set {
                    foreach (var i in value) {
                        ProdutorasItens.Add(new SelectListItem() { Text = i.Nome, Value = i.Nome });
                    }
                }
            }

            public List<string> Categorias { get; set; }
        }

    }


}
