
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 06/01/2017 11:09:03
-- Generated from EDMX file: C:\Users\fmunh\Source\Repos\exemplo-mvc-ef\MiniAulaENG\Models\Model.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Database];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_CategoriaFilme_Categoria]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CategoriaFilme] DROP CONSTRAINT [FK_CategoriaFilme_Categoria];
GO
IF OBJECT_ID(N'[dbo].[FK_CategoriaFilme_Filme]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CategoriaFilme] DROP CONSTRAINT [FK_CategoriaFilme_Filme];
GO
IF OBJECT_ID(N'[dbo].[FK_ProdutoraFilme]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Filmes] DROP CONSTRAINT [FK_ProdutoraFilme];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Filmes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Filmes];
GO
IF OBJECT_ID(N'[dbo].[Categorias]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Categorias];
GO
IF OBJECT_ID(N'[dbo].[Produtoras]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Produtoras];
GO
IF OBJECT_ID(N'[dbo].[CategoriaFilme]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CategoriaFilme];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Filmes'
CREATE TABLE [dbo].[Filmes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nome] nvarchar(max)  NOT NULL,
    [Produtora_Id] int  NOT NULL
);
GO

-- Creating table 'Categorias'
CREATE TABLE [dbo].[Categorias] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nome] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Produtoras'
CREATE TABLE [dbo].[Produtoras] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nome] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'CategoriaFilme'
CREATE TABLE [dbo].[CategoriaFilme] (
    [Categoria_Id] int  NOT NULL,
    [Filmes_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Filmes'
ALTER TABLE [dbo].[Filmes]
ADD CONSTRAINT [PK_Filmes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Categorias'
ALTER TABLE [dbo].[Categorias]
ADD CONSTRAINT [PK_Categorias]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Produtoras'
ALTER TABLE [dbo].[Produtoras]
ADD CONSTRAINT [PK_Produtoras]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Categoria_Id], [Filmes_Id] in table 'CategoriaFilme'
ALTER TABLE [dbo].[CategoriaFilme]
ADD CONSTRAINT [PK_CategoriaFilme]
    PRIMARY KEY CLUSTERED ([Categoria_Id], [Filmes_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Categoria_Id] in table 'CategoriaFilme'
ALTER TABLE [dbo].[CategoriaFilme]
ADD CONSTRAINT [FK_CategoriaFilme_Categoria]
    FOREIGN KEY ([Categoria_Id])
    REFERENCES [dbo].[Categorias]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Filmes_Id] in table 'CategoriaFilme'
ALTER TABLE [dbo].[CategoriaFilme]
ADD CONSTRAINT [FK_CategoriaFilme_Filme]
    FOREIGN KEY ([Filmes_Id])
    REFERENCES [dbo].[Filmes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CategoriaFilme_Filme'
CREATE INDEX [IX_FK_CategoriaFilme_Filme]
ON [dbo].[CategoriaFilme]
    ([Filmes_Id]);
GO

-- Creating foreign key on [Produtora_Id] in table 'Filmes'
ALTER TABLE [dbo].[Filmes]
ADD CONSTRAINT [FK_ProdutoraFilme]
    FOREIGN KEY ([Produtora_Id])
    REFERENCES [dbo].[Produtoras]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ProdutoraFilme'
CREATE INDEX [IX_FK_ProdutoraFilme]
ON [dbo].[Filmes]
    ([Produtora_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------