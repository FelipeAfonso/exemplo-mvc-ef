﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MiniAulaENG.Models;
using static MiniAulaENG.Models.FilmesViewModel;

namespace MiniAulaENG.Controllers {
    public class FilmesController : Controller {
        private ModelContainer db = new ModelContainer();

        // GET: Filmes
        public ActionResult Index() {

            return View(db.Filmes.ToList());
        }

        // GET: Filmes/Details/5
        public ActionResult Details(int? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Filme filme = db.Filmes.Find(id);
            if (filme == null) {
                return HttpNotFound();
            }
            return View(filme);
        }

        // GET: Filmes/Create
        public ActionResult Create() {
            var model = new CreateFilmeViewModel();
            var plist = new List<Produtora>();
            foreach (var i in db.Categorias.ToList()) {
                model.Categorias.Add(i.Nome);
            }
            foreach (var i in db.Produtoras.ToList()) {
                plist.Add(i);
            }
            model.Produtoras = plist;
            return View(model);
        }

        // POST: Filmes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateFilmeViewModel model) {
            var produtora = db.Produtoras.SingleOrDefault(o => o.Nome == model.Produtora);
            db.Produtoras.Attach(produtora);
            db.Filmes.Add(new Filme() { Nome = model.Nome, Produtora = produtora });
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Filmes/Edit/5
        public ActionResult Edit(int? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Filme filme = db.Filmes.Find(id);
            if (filme == null) {
                return HttpNotFound();
            }
            return View(filme);
        }

        // POST: Filmes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nome")] Filme filme) {
            if (ModelState.IsValid) {
                db.Entry(filme).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(filme);
        }

        // GET: Filmes/Delete/5
        public ActionResult Delete(int? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Filme filme = db.Filmes.Find(id);
            if (filme == null) {
                return HttpNotFound();
            }
            return View(filme);
        }

        // POST: Filmes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id) {
            Filme filme = db.Filmes.Find(id);
            db.Filmes.Remove(filme);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
