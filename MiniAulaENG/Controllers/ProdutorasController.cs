﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MiniAulaENG.Models;

namespace MiniAulaENG.Controllers
{
    public class ProdutorasController : Controller
    {
        private ModelContainer db = new ModelContainer();

        // GET: Produtoras
        public ActionResult Index()
        {
            return View(db.Produtoras.ToList());
        }

        // GET: Produtoras/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Produtora produtora = db.Produtoras.Find(id);
            if (produtora == null)
            {
                return HttpNotFound();
            }
            return View(produtora);
        }

        // GET: Produtoras/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Produtoras/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nome")] Produtora produtora)
        {
            if (ModelState.IsValid)
            {
                db.Produtoras.Add(produtora);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(produtora);
        }

        // GET: Produtoras/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Produtora produtora = db.Produtoras.Find(id);
            if (produtora == null)
            {
                return HttpNotFound();
            }
            return View(produtora);
        }

        // POST: Produtoras/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nome")] Produtora produtora)
        {
            if (ModelState.IsValid)
            {
                db.Entry(produtora).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(produtora);
        }

        // GET: Produtoras/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Produtora produtora = db.Produtoras.Find(id);
            if (produtora == null)
            {
                return HttpNotFound();
            }
            return View(produtora);
        }

        // POST: Produtoras/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Produtora produtora = db.Produtoras.Find(id);
            db.Produtoras.Remove(produtora);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
